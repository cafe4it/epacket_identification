module.exports = {
    SHOW_PAGE_ACTION: 'SHOW_PAGE_ACTION',
    SK: 'VnYZvQVf',
    AF: '337019',
    EPACKET: 'EPACKET',
    QUERY_SINGLE: 'QUERY_SINGLE',
    QUERY_MULTIPLE: 'QUERY_MULTIPLE',
    SHIPPING_COMPANIES: [
        {id: 'EMS_ZX_ZX_US', name: 'ePacket'},
        {id: 'SF_EPARCEL', name: 'SF eParcel'},
    ],
    TTL: 60 * 60 * 24 * 3,
    DEFAULT_SETTINGS: {
        selectedCountry: 'Default',
        selectedCountries: ['US', 'UK', 'AU'],
        isMultipleCountry: false,
        selectedCurrency: 'USD',
        selectedCompany: 'EMS_ZX_ZX_US'
    },
    UPDATE_SETTINGS_TO_TAB: 'UPDATE_SETTINGS_TO_TAB',
    IS_DEVELOPMENT: process.env.NODE_ENV === "development",
    SERVER_URL: process.env.NODE_ENV === "development" ? 'http://localhost:8000' : 'http://45.63.21.9:80'
}
