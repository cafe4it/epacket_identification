onmessage = function(e){
	var request = new XMLHttpRequest();
	request.onreadystatechange = function(){
		if(request.readyState == 4 && request.status < 400){
			postMessage(request.response);
		}
	}
	request.open(e.data.method, e.data.uri);
    request.send();
}