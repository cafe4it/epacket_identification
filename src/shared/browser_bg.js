import {denodeify} from './denodeify'
import {storage, runtime} from './browser'
import pThrottle from 'p-throttle'
const RSVP = require('rsvp')
const cookies = {
    remove: denodeify(chrome.cookies.remove)
}

const tabs = {
    create: denodeify(chrome.tabs.create),
    update: denodeify(chrome.tabs.update),
    sendMessage: denodeify(chrome.tabs.sendMessage)
}

const rate = {
    send: pThrottle(function (options) {
       return myWebWorker(options)
    },50, 1000),
	request: pThrottle(function (options) {
		return myWebWorker(options)
	},100, 1000)
}

const myWebWorker = (options) => {
	const promise = new RSVP.Promise((resolve, reject) => {
		try{
            let request = new XMLHttpRequest();
            request.onreadystatechange = function(){
                if(request.readyState == 4 && request.status < 400){
                    resolve(request.response);
                }
            }
            request.open(options.method, options.uri);
            if(options.data){
                request.send(JSON.stringify(options.data))
            }else{
                request.send();
            }
		}catch(ex){
			reject(ex)
		}
	})
    return promise
}

const myRequest = (options) => {
	return new RSVP.Promise((resolve, reject) => {
		try{
			const request = new Request(options.uri, {
				method: options.method,
				redirect: 'follow',
				headers: new Headers({
					'Content-Type': 'application/json'
				})
			})
			fetch(request).then(function(response){
				if(response.status < 400){
					return response.json()
				}
			}).then(function (json) {
				resolve(json || null)
			})
		}catch (ex){
			reject(ex)
		}
	})
}

const webWorker = {
	send: rate.send,
	request: rate.request
}

export {storage, runtime, cookies, tabs, webWorker}