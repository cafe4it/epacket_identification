import {denodeify, denodeify_} from './denodeify'

export const storage = {
	local: {
		get: denodeify(chrome.storage.local.get),
		set: denodeify(chrome.storage.local.set),
		remove: denodeify(chrome.storage.local.remove)
	},
	sync: {
		get: denodeify(chrome.storage.sync.get),
		set: denodeify(chrome.storage.sync.set),
		remove: denodeify(chrome.storage.sync.remove),
	}
}

export const runtime = {
	sendMessage: denodeify(chrome.runtime.sendMessage),
	getBackgroundPage: denodeify_(chrome.runtime.getBackgroundPage)
}