import Vue from 'vue'
import vueResource from 'vue-resource'
import iView from 'iview';
import App from './App.vue'

Vue.use(iView);
Vue.use(vueResource)

new Vue({
	el: '#app',
	render: h => h(App)
})
