import _each from 'lodash/each'
import _get from 'lodash/get'
import _find from 'lodash/find'
import _rnd from 'lodash/random'
import _assign from 'lodash/assign'
import _kebabCase from 'lodash/kebabCase'
import {runtime} from '../shared/browser'
import C from '../shared/constants'
import './index.css'

let settings = window.localStorage.getItem('_s3tt1ngs_') ? JSON.parse(window.localStorage.getItem('_s3tt1ngs_')) : undefined

runtime.sendMessage({
    action: C.SHOW_PAGE_ACTION,
    data: null
}).then((res) => {
    if(res && res['settings']){
        res['settings'] = _assign(C.DEFAULT_SETTINGS, res['settings'])
        window.localStorage.setItem('_s3tt1ngs_', JSON.stringify(res['settings']))
    }
});
const selectedCountry = _get(settings, 'selectedCountry');
const selectedCountries = _get(settings, 'selectedCountries') || [];
const selectedCurrency = _get(settings, 'selectedCurrency');
const selectedCompany = _get(settings, 'selectedCompany') || 'EMS_ZX_ZX_US';
console.log(selectedCompany)
const shippingCompany = _find(C.SHIPPING_COMPANIES, {id: selectedCompany});
const _companyName = _kebabCase(shippingCompany.name);
const isMultipleCountry = _get(settings, 'isMultipleCountry') || false;

$(document).on('ready', () => {
    try{
        let gr_products = $('li[qrdata], li[data-product-id], li.item');
        // console.log(gr_products.length)
        gr_products.map((i, e) => {
            let product_id = null
            const qrdata = e.getAttribute('qrdata')
            const data_product_id = e.getAttribute('data-product-id')
            const atc_product_id = $(e).find('input.atc-product-id')[0]
            const pic_rind = $(e).find('a.pic-rind, a.img')[0]
            if (qrdata) {
                product_id = qrdata.split('|')[1]
            }
            if (data_product_id) {
                product_id = data_product_id
            }
            if (product_id == null && atc_product_id) {
                product_id = atc_product_id.value
            }
            if (product_id == null && pic_rind && window.location.href.match(/\/store\//)){
                let _href = pic_rind.getAttribute('href')
                _href = _href.match(/_(.*).html/)
                if(_href && _href.length > 0){
                    product_id = _href[1]
                }
            }
            Query_Company(product_id, e)
        })
    }catch (e){

    }

})


function Query_Company(product_id, e){
    try{
        let shipToCountry = document.getElementById('shipCountry') ? document.getElementById('shipCountry').value : null
        if (!shipToCountry) {
            shipToCountry = window.localStorage['freight_country_code'] ? window.localStorage['freight_country_code'].replace(/"/g, '') : null
        }
        if (!shipToCountry){
            shipToCountry = 'US'
        }
        let currency = document.querySelector('.currency') ? document.querySelector('.currency').textContent : 'USD';



        if(selectedCountry && selectedCountry !== 'Default'){
            shipToCountry = selectedCountry;
        }

        if(selectedCurrency && selectedCountry !== ''){
            currency = selectedCurrency;
        }

        if(product_id && currency){
            if(!isMultipleCountry){
                const companyId = `ALI.${_companyName}_${product_id}_${currency}_${shipToCountry}`
                if(shipToCountry){
                    if (!sessionStorage[companyId]) {
                        runtime.sendMessage({
                            action: C.QUERY_SINGLE,
                            data: {id: companyId, product_id, shipToCountry, currency, selectedCompany, timer: Date.now() + _rnd(0, 100)}
                        }).then((res) => {
                            if(res && res.id){
                                sessionStorage[res.id] = JSON.stringify(res)
                                showSingle(companyId, e)
                            }
                        })
                    }else{
                        showSingle(companyId, e)
                    }
                }
            }else if(isMultipleCountry && selectedCountries.length > 0){
                const companyGroupId = `ALI.${_companyName}_${product_id}_${currency}`
                const data = []
                const ids = []
                _each(selectedCountries, (c) => {
                    const companyId = `ALI.${_companyName}_${product_id}_${currency}_${c}`
                    if(!sessionStorage[companyId]){
                        data.push({
                            id: companyId,
                            product_id,
                            shipToCountry: c,
                            selectedCompany,
                            currency,
                            timer: Date.now() + _rnd(0, 100)
                        })
                    }
                    ids.push(companyId)
                });
                if(data.length > 0){
                    runtime.sendMessage({
                        action: C.QUERY_MULTIPLE,
                        data: data
                    })
                }
                showMultiple(companyGroupId, ids, e)
            }
        }
    }catch (e) {
        console.error(e)
    }

}

function showMultiple(companyGroupId, companyIds, container) {
    const tool = document.createElement('div')
    let item_container = container.querySelector('div.item, div.p4p-item-block, div.img-container, div.img')
    if(window.location.href.match(/\/store\//)){
        item_container = container
    }
    tool.className = 'oi0ioi-tool multiple-c0untries'
    tool.setAttribute('id', companyGroupId)
    const companyText = document.createElement('span')
    companyText.textContent = shippingCompany.name;
    companyText.className = 'c0mpany_text'
    tool.appendChild(companyText)
    const resultGroup = document.createElement('div')
    resultGroup.className = 'resultGr0up'
    _each(companyIds, (id) => {
        const item = document.createElement('p')
        item.setAttribute('id', id)
        let rs = sessionStorage[id]
        if(rs){
            rs = JSON.parse(rs)
            item.textContent = `${_get(rs, 'shipToCountry.n')}\t${rs.price ? ' - ' + rs.price : ' - none'}`
            item.className = `resultIt3m ${rs.className}`
        }else{
            item.textContent = 'loading...'
            item.className = 'resultIt3m'
        }
        resultGroup.appendChild(item)
    })
    // tool.appendChild(resultGroup)
    if (item_container) {
        $(tool).mouseover((e) => {
            resultGroup.style.display = 'block'
        })
        $(tool).mouseout((e) => {
            resultGroup.style.display = 'none'
        })
        const topNode = item_container.children[0]
        item_container.insertBefore(resultGroup, topNode)
        item_container.insertBefore(tool, resultGroup)
    }
}

function showSingle(companyId, container) {
    const tool = document.createElement('div')
    let item_container = container.querySelector('div.item, div.p4p-item-block, div.img-container, div.img')
    if(window.location.href.match(/\/store\//)){
        item_container = container
    }
    let shipToCountryFlagClassName = document.querySelector('span.ship-to i').className
    const company = JSON.parse(sessionStorage[companyId])


    const shipToCountry = _get(company, 'shipToCountry');

    if(shipToCountry){
        shipToCountryFlagClassName = `css_flag css_${shipToCountry.c.toLowerCase()}`;
    }
    tool.className = `oi0ioi-tool ${company.className}`
    const company_node = document.createElement('div')
    company_node.className = 'c0mpany_info'
    company_node.textContent = `${company.name}${company.price ? ' - ' + company.price : ''}`
    tool.appendChild(company_node)
    const flag_node = document.createElement('div')
    flag_node.className = `flag_node ${shipToCountryFlagClassName}`
    tool.appendChild(flag_node)
    const clear_node = document.createElement('div')
    clear_node.style.display = 'both'
    tool.appendChild(clear_node)
    if (item_container) {
        const topNode = item_container.children[0]
        item_container.insertBefore(tool, topNode)
    }
}

chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    switch (msg.action){
        case C.UPDATE_SETTINGS_TO_TAB:
            if(msg.data){
                window.localStorage.setItem('_s3tt1ngs_', JSON.stringify(msg.data))
            }
            break;
        case C.QUERY_MULTIPLE:
            if(msg.data){
               _each(msg.data, (d) => {
                   if(d && d.id){
                       sessionStorage[d.id] = JSON.stringify(d)
                       const item = document.getElementById(d.id)
                       if(item){
                           item.textContent = `${_get(d, 'shipToCountry.n')}\t${d.price ? ' - ' + d.price : ' - none'}`
                           item.className = `resultIt3m ${d.className}`
                       }
                   }
               })
            }
            break;
    }
})
