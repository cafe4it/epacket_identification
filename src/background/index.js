import {webWorker, tabs, cookies, storage} from '../shared/browser_bg'
import C from '../shared/constants'
import _ from 'lodash'
import RSVP from 'rsvp'
const uri_ship_tpl = _.template('https://freight.aliexpress.com/ajaxFreightCalculateService.htm?productid=<%=product_id%>&count=1&currencyCode=<%=currency%>&sendGoodsCountry=&country=<%=shipToCountry%>&province=&city=&abVersion=1&_=<%=timer%>')
const _AnalyticsCode = 'UA-74453743-7';
let service, tracker;

let tabIds = []
let SHIPPING_COUNTRIES = null;

const importScript = (function (oHead) {
    function loadError(oError) {
        throw new URIError("The script " + oError.target.src + " is not accessible.");
    }

    return function (sSrc, fOnload) {
        var oScript = document.createElement("script");
        oScript.type = "text\/javascript";
        oScript.onerror = loadError;
        if (fOnload) {
            oScript.onload = fOnload;
        }
        oHead.appendChild(oScript);
        oScript.src = sSrc;
    }

})(document.head || document.getElementsByTagName("head")[0]);

importScript(chrome.runtime.getURL('shared/google-analytics-bundle.js'), function () {
    console.info('google analytics platform loaded...');
    service = analytics.getService('aliexpress_epacket_identification');
    tracker = service.getTracker(_AnalyticsCode);
    tracker.sendAppView('App view');
    window.tracker = tracker;
});

chrome.storage.sync.get(['_HELP_PAGE_', '_SETTINGS_', 'SHIPPING_COUNTRIES'], function (rs) {
    // let help_page = rs && rs['_HELP_PAGE_']
    let settings = rs && rs['_SETTINGS_']
    SHIPPING_COUNTRIES = rs && rs['SHIPPING_COUNTRIES']
    if(!settings){
        settings = C.DEFAULT_SETTINGS
        chrome.storage.sync.set({['_SETTINGS_']: settings})
        window.localStorage.setItem('settings', JSON.stringify(settings))
    }

    if(!SHIPPING_COUNTRIES){
        webWorker.send({
            method: 'GET',
            uri: 'https://m.aliexpress.com/GetCountryList.htm'
        }).then((response) => {
            response = JSON.parse(response)
            SHIPPING_COUNTRIES = _.chain(response).get('countries').filter((c) => c['i'] === undefined).uniqBy('c').value()
            if(SHIPPING_COUNTRIES.length > 0){
                storage.sync.set({['SHIPPING_COUNTRIES']: SHIPPING_COUNTRIES});
                window.localStorage.setItem('countries', JSON.stringify(SHIPPING_COUNTRIES));
            }
        })
    }
})

chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    const {action, data} = msg
    const senderTabId = sender.tab.id
    let settings = window.localStorage.getItem(['settings'])
    if(settings) {
        settings = JSON.parse(settings)
    }
    switch (action) {
        case C.SHOW_PAGE_ACTION:
            chrome.pageAction.show(senderTabId)
	        _upsert(senderTabId)
            if(settings){
                sendResponse({settings})
            }
            break;
        case C.QUERY_SINGLE:
            if(data && data.id){
                get_company(data).then((res) => {
                    sendResponse(res)
                })
            }
            break;
        case C.QUERY_MULTIPLE:
            const newData = _.filter(data, (d) => { return d && d.id})
            get_companies(newData, senderTabId);
            break;
    }
    return true;
})

function get_companies(data, tabId) {
    try {
        const requestIds = _.map(data, (d) => { return d.id})
        webWorker.request({
            method: 'POST',
            uri: `${C.SERVER_URL}/get_m`,
            data: requestIds
        }).then((res) => {
            res = JSON.parse(res)
            const responseIds = []
            const responseItems = _.chain(res).filter((i) => i !== null).map((i) => {
                i = JSON.parse(i)
                responseIds.push(i.id)
                return i;
            }).value();
            const notExitsIds = _.xor(requestIds, responseIds);
            if(responseItems.length > 0){
                tabs.sendMessage(tabId, {
                    action: C.QUERY_MULTIPLE,
                    data: responseItems
                })
            }
            if(notExitsIds.length > 0) {
                const promises = _.chain(data).filter((i) => notExitsIds.indexOf(i.id) !== -1).map((i) => find_company_info(i)).value()
                RSVP.all(promises).then((resItems) => {
                    if(resItems.length > 0){
                        tabs.sendMessage(tabId, {
                            action: C.QUERY_MULTIPLE,
                            data: resItems
                        })
                        webWorker.request({
                            method: 'POST',
                            data: resItems,
                            uri: `${C.SERVER_URL}/post_m`
                        }).then((res) => console.log(res))
                    }
                })
            }
        })
    }catch (ex) {
        console.log(ex)
    }
}

function get_company(data) {
    return new RSVP.Promise((resolve, reject) => {
        try{
            webWorker.request({
                uri: `${C.SERVER_URL}/get/${data.id}`,
                method: 'GET'
            }).then((res) => {
                res = JSON.parse(res);
                if (res && res.id) {
                    resolve(res);
                } else {
                    query_company(data).then((res) => {
                        if (res && res.id) {
                            resolve(res);
                        }
                    })
                }
            })
        }catch (ex) {
            console.error(ex)
            reject(ex)
        }
    })
}

function find_company_info(data) {
    return new RSVP.Promise((resolve, reject) => {
        const uri_company = uri_ship_tpl(data)
        webWorker.send({
            method: 'GET',
            uri: uri_company
        }).then((res) => {
            try{
                const company = _.find(C.SHIPPING_COMPANIES, {id: data['selectedCompany']})
                const _data = JSON.parse(res.slice(1, -1))
                const result = _.find(_data['freight'], {company: company.id})
                const resData = {
                    id: data['id'],
                    name: `No ${company.name}`,
                    className: 'oi0ioi_no_company',
                    shipToCountry: _.find(SHIPPING_COUNTRIES ,{c: data['shipToCountry']}),
                    ttl: C.TTL
                }
                if (result) {
                    resData['name'] = result['companyDisplayName']
                    resData['className'] = 'oi0ioi_company_has_price'
                    resData['price'] = result['localPriceFormatStr']
                    resData['isTracked'] = result['isTracked']
                    if (result['price'] === "0") {
                        resData['price'] = result['status'].toUpperCase()
                        resData['className'] = 'oi0ioi_company'
                    }
                }
                return resolve(resData)
            }catch (ex){
                console.error(ex)
                reject(ex)
            }
        })
    })
}

function query_company(data) {
    return new RSVP.Promise((resolve, reject) => {
        try{
            find_company_info(data).then((resData) => {
                resolve(resData)
                webWorker.request({
                    data: resData,
                    method: 'POST',
                    uri: `${C.SERVER_URL}/post`
                }).then((res) => {
                    if(C.IS_DEVELOPMENT){
                        if(res && res.id){
                            console.info(resData.id, 'OK')
                        }
                    }
                })
            })
        }catch (e) {
            reject(e)
        }
    })
}


chrome.storage.onChanged.addListener(function (changes, areaName) {
    if(areaName === 'sync'){
        if(_.has(changes, '_SETTINGS_')){
            const settings = _.get(changes, '_SETTINGS_.newValue')
            window.localStorage.setItem('settings', JSON.stringify(settings))
			tabIds.forEach((id) => {
            	tabs.sendMessage(id, {
            		action: C.UPDATE_SETTINGS_TO_TAB,
		            data: settings
	            })
			})
        }
    }
})

function _upsert(newVal){
	tabIds.push(newVal)
	tabIds = _.uniq(tabIds)
}
